<?php

namespace controller;

use Exception;
use model\Usuario;

class CadastroController {
    function view() {
        require_once './view/Cadastro/index.php';
    }

    function cadastrar() {
        if (empty($_POST['usuario'])) {
            throw new Exception('Usuário/Senha não informado');
        }

        if (empty($_POST['senha'])) {
            throw new Exception('Usuário/Senha não informado');
        }

        if (empty($_POST['confirmarSenha'])) {
            throw new Exception('Usuário/Senha não informado');
        }

        $usuario = $_POST['usuario'];
        $senha = $_POST['senha'];
        $confirmarSenha = $_POST['confirmarSenha'];

        $model = new Usuario();

        $retorno = $model->cadastrar($usuario, $senha, $confirmarSenha);

        if ($retorno == true) {
            $_SESSION['msgLogin'] = 'Usuário cadastrado.';
            header("Location: /login");
            exit;
        }

        $_SESSION['msgCadastro'] = $model->getErro();
        header("Location: /cadastro");
    }
}

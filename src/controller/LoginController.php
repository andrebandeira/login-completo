<?php

namespace controller;

use model\Login;
use Exception;
use model\Usuario;

class LoginController {
    function view() {
        require_once './view/Login/index.php';
    }

    function entrar() {
        if (empty($_POST['usuario'])) {
            throw new Exception('Usuário/Senha não informado');
        }

        if (empty($_POST['senha'])) {
            throw new Exception('Usuário/Senha não informado');
        }

        $usuario = $_POST['usuario'];
        $senha = $_POST['senha'];

        $model = new Usuario();
        $retorno = $model->logar($usuario, $senha);

        if ($retorno) {
            $_SESSION['usuario'] = $usuario;
            header("Location: /home");
            exit;
        }

        $_SESSION['msgLogin'] = $model->getErro();
        header("Location: /login");
    }

    function logout() {
        if (!empty($_SESSION['usuario'])) {
            unset($_SESSION['usuario']);
        }

        header("Location: /login");
    }
}
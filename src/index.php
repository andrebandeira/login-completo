<?php
try {
    spl_autoload_register(function ($class) {
        require_once(str_replace('\\', '/', $class . '.php'));
    });

    $excecoes = [
        '/login',
        '/login/entrar',
        '/cadastro',
        '/cadastro/novo',
    ];

    $url = $_SERVER['REQUEST_URI'];
	session_start();

	if (empty($_SESSION['usuario']) && !in_array($url,  $excecoes)) {
		header("Location: /login");
		exit();
	}

	require_once 'routes.php';
} catch (Exception $ex) {
    echo $ex->getMessage();
}



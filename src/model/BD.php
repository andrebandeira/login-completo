<?php

namespace model;

use Exception;
use mysqli;

class BD {
    private $__servidor = "db:3306";
    private $__usuario = "aula";
    private $__senha = "aula";
    private $__dbname = "login";
    private $__conn;

    public function conecta() {
        $this->__conn = new mysqli($this->__servidor, $this->__usuario, $this->__senha, $this->__dbname);

        if(!$this->__conn){
            throw new Exception($this->__conn->connect_error);
        }
    }

    public function insert(string $sql, array $params) {
        $stmt = $this->__conn->prepare($sql);

        if (!$stmt) {
            throw new Exception($this->__conn->error);
        }

        $types = str_repeat('s', count($params));
        $stmt->bind_param($types, ...$params);

        $result = $stmt->execute();

        if (!$result) {
            throw new Exception($stmt->error);
        }

        $stmt->fetch();

        $stmt->close();
    }

    public function select(string $sql, array $params) {
        $result = null;

        $stmt = $this->__conn->prepare($sql);

        if (!$stmt) {
            throw new Exception($this->__conn->error);
        }

        $types = str_repeat('s', count($params));
        $stmt->bind_param($types, ...$params);

        $stmt->execute();

        $stmt->bind_result($result);

        $stmt->fetch();

        $stmt->close();

        return $result;
    }

}
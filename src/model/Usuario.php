<?php

namespace  model;

class Usuario {
    private $__erro;
    private $__bd;

    public function __construct()
    {
        $this->__bd = new BD();
        $this->__bd->conecta();
    }

    public function cadastrar(String $usuario, String $senha, String $confirmacaoSenha) : bool {
        if (empty($usuario)) {
            $this->__erro = 'Usuário obrigatório.';
            return false;
        }

        if (empty($senha)) {
            $this->__erro = 'Senha obrigatório.';
            return false;
        }

        if (empty($confirmacaoSenha)) {
            $this->__erro = 'Confirmação de senha obrigatório.';
            return false;
        }

        if ($senha != $confirmacaoSenha) {
            $this->__erro = 'Senha e confirmação de senha não são iguais.';
            return false;
        }

        $sql = 'Insert into usuarios (usuario, senha) values (?, ?)';

        $this->__bd->insert($sql, [$usuario, password_hash($senha, PASSWORD_BCRYPT)]);

        return true;
    }

    public function logar(String $usuario, String $senha) : bool {
        if (empty($usuario)) {
            $this->__erro = 'Usuário obrigatório.';
            return false;
        }

        if (empty($senha)) {
            $this->__erro = 'Senha obrigatória.';
            return false;
        }

        $sql = 'Select senha
                from   usuarios
                where  usuario = ?';

        $senhaBD = $this->__bd->select($sql, [$usuario]);

        if (!$senhaBD) {
            $this->__erro = 'Usuário/Senha não encontrado';
            return false;
        }

        $senhaCorreta = password_verify($senha, $senhaBD);

        if (!$senhaCorreta) {
            $this->__erro = 'Usuário/Senha não encontrado';
            return false;
        }

        return true;
    }

    public function getErro() {
        return $this->__erro;
    }
}
<?php
use controller\LoginController;
use controller\HomeController;
use controller\CadastroController;
use controller\NotFoundController;

$url = $_SERVER['REQUEST_URI'];

switch ($url) {
    case '/login':
        $controller = new LoginController();
        $controller->view();
        return;
    case '/login/entrar':
        $controller = new LoginController();
        $controller->entrar();
        return;
    case '/':
    case '/home':
        $controller = new HomeController();
        $controller->view();
        return;
    case '/logout':
        $controller = new LoginController();
        $controller->logout();
        return;
    case '/cadastro':
        $controller = new CadastroController();
        $controller->view();
        return;
    case '/cadastro/novo':
        $controller = new CadastroController();
        $controller->cadastrar();
        return;
}

$controller = new NotFoundController();
$controller->view();





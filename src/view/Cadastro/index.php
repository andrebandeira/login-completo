<!DOCTYPE html>
<head>
    <meta charset="UTF-8" />
    <title>Aula Login</title>
</head>
<body>
<form method="post" action="/cadastro/novo">
    <h1>Login</h1>
    <?php
    if (!empty($_SESSION['msgCadastro'])) {
        echo '<h4>'.$_SESSION['msgCadastro'].'</h4>';
        unset($_SESSION['msgCadastro']);
    }
    ?>
    <p>
        <label for="usuario">Usuario</label>
        <input id="usuario" name="usuario" required type="text"/>
    </p>

    <p>
        <label for="senha">Senha</label>
        <input id="senha" name="senha" required type="password" />
    </p>

    <p>
        <label for="confirmarSenha">Confirmar Senha</label>
        <input id="confirmarSenha" name="confirmarSenha" required type="password" />
    </p>

    <p>
        <input type="submit" value="Cadastrar" />
    </p>
</form>
</body>
<!DOCTYPE html>
<head>
    <meta charset="UTF-8" />
    <title>Aula Login</title>
</head>
<body>
    <form method="post" action="/login/entrar">
        <h1>Login</h1>
        <?php
            if (!empty($_SESSION['msgLogin'])) {
               echo '<h4>'.$_SESSION['msgLogin'].'</h4>';
               unset($_SESSION['msgLogin']);
            }
        ?>
        <p>
            <label for="usuario">Usuario</label>
            <input id="usuario" name="usuario" required type="text"/>
        </p>

        <p>
            <label for="senha">Senha</label>
            <input id="senha" name="senha" required type="password" />
        </p>

        <p>
            <input type="submit" value="Entrar" />
        </p>

        <p>
            Ainda não tem conta?
            <a href="/cadastro">Cadastre-se</a>
        </p>
    </form>
</body>
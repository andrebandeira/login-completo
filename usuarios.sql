CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(100) NOT NULL UNIQUE,
  `senha` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
);
